package com.assesment.task.viewModel

import android.arch.lifecycle.MutableLiveData
import android.databinding.ObservableArrayList
import android.databinding.ObservableList
import com.assesment.task.BR
import com.assesment.task.TaskApplicationClass
import com.assesment.task.R
import com.assesment.task.base.BaseViewModel
import com.assesment.task.dataManager.OperationalDataManager
import com.assesment.task.dataManager.ResponseListener
import com.assesment.task.model.ErrorResponse
import com.assesment.task.model.Articles
import com.assesment.task.utils.Constants
import com.assesment.task.view.listeners.OnItemClickListener
import me.tatarka.bindingcollectionadapter2.ItemBinding
import javax.inject.Inject

/**
 * ViewModel Class for Article List Fragment
 * Author: Muhammad Mudasir
 */
class ArticleListFragmentViewModel : BaseViewModel() {

    val articleItemViewModel: ObservableList<ArticleItemViewModel> = ObservableArrayList<ArticleItemViewModel>()

    val userClickResponse: MutableLiveData<String> = MutableLiveData()

    /**
     * Recycler View binding using Binding Collection adapter library
     */
    val singleItem: ItemBinding<ArticleItemViewModel>? = ItemBinding.of<ArticleItemViewModel>(BR.itemViewModel, R.layout.article_item_layout)
            .bindExtra(BR.listener, object : OnItemClickListener {
                override fun onItemClick(item: Any) {
                    userClickResponse.value = (item as ArticleItemViewModel).position
                }
            })

    @Inject
    lateinit var operationalDataManager: OperationalDataManager

    /**
     * Initialization block to inject the OperationalDataManager object
     */
    init {

        TaskApplicationClass.getAppComponent().inject(this)
    }

    /**
     * Method to initiate the get articleList request
     */
    fun getArticleList() {
        operationalDataManager.articleServiceRequest(getArticlesParams(), Articles::class.java, object : ResponseListener<Articles> {

            override fun onResponse(result: Articles?, error: ErrorResponse?) {
                loading.value = true
                if (null != result)
                    serviceResponse.value = result
                else {
                    serviceErrorResponse.value = error
                }
            }
        })
    }

    /**
     * Method which renders the articles data into recycler view
     * @param articleList - Articles data
     */
    fun renderArticleList(articleList: Articles) {
        articleItemViewModel.clear()
        var articleItemViewModel: ArticleItemViewModel

        for ((index, items) in articleList.articles.withIndex()) {
            articleItemViewModel = ArticleItemViewModel()
            articleItemViewModel.position = index.toString()
            articleItemViewModel.articleTitle.value = items.title
            articleItemViewModel.articleAbstract.value = items.abstract
            articleItemViewModel.articleByLine.value = items.byline
            articleItemViewModel.articlePublishedDate.value = items.publishedDate
            this.articleItemViewModel.add(articleItemViewModel)
        }
    }

    /**
     * Method to construct the Article Service parameters
     * @return - Parameter Map
     */
    private fun getArticlesParams(): MutableMap<String, String> {

        val data = HashMap<String, String>()
        data[Constants.KEY_API_KEY] = Constants.VALUE_API_KEY
        return data
    }

}