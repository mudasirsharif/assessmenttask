package com.assesment.task.viewModel

import android.arch.lifecycle.ViewModel
import com.assesment.task.base.BaseModel
import com.assesment.task.utils.Constants

/**
 * SharedViewModel Class which can be shared between fragments
 * Author: Muhammad Mudasir
 */
class SharedViewModel : ViewModel() {

    var model: BaseModel = BaseModel()
    var itemPosition: Int = 0

}