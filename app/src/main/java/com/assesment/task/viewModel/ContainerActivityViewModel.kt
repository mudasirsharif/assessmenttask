package com.assesment.task.viewModel

import com.assesment.task.base.BaseViewModel

/**
 * ViewModel Class for Container Activity
 * Author: Muhammad Mudasir
 */
class ContainerActivityViewModel : BaseViewModel()