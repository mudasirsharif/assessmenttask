package com.assesment.task.viewModel

import android.arch.lifecycle.MutableLiveData
import com.assesment.task.base.BaseViewModel
import com.assesment.task.model.Article
import android.webkit.WebView
import android.databinding.BindingAdapter
import android.webkit.WebViewClient


/**
 * ViewModel Class for Map Fragment
 * Author: Muhammad Mudasir
 */
class ArticleFragmentViewModel : BaseViewModel() {

    lateinit var articleList: List<Article>
    var articlePosition: Int = 0

    var articleTitle: MutableLiveData<String> = MutableLiveData()
    var articleSection: MutableLiveData<String> = MutableLiveData()
    var articleSource: MutableLiveData<String> = MutableLiveData()
    var articleAbstract: MutableLiveData<String> = MutableLiveData()
    var articleViews: MutableLiveData<String> = MutableLiveData()
    var byLine: MutableLiveData<String> = MutableLiveData()
    var publishedDate: MutableLiveData<String> = MutableLiveData()
    var articleUrl: String = ""

    fun renderArticleData() {

        toolBarTitle.value = articleList[articlePosition].title
        articleTitle.value = articleList[articlePosition].title
        articleAbstract.value = articleList[articlePosition].abstract
        articleSection.value = "Section: " + articleList[articlePosition].section
        articleSource.value = "Source: " + articleList[articlePosition].source
        articleUrl = articleList[articlePosition].url
        articleViews.value = articleList[articlePosition].views.toString()
        byLine.value = articleList[articlePosition].byline
        publishedDate.value = articleList[articlePosition].publishedDate

    }

}