package com.assesment.task.viewModel

import android.arch.lifecycle.MutableLiveData
import com.assesment.task.base.BaseViewModel

/**
 * ViewModel Class for Article Item in Recycler View
 * Author: Muhammad Mudasir
 */
class ArticleItemViewModel : BaseViewModel() {

    lateinit var position: String
    var articleTitle: MutableLiveData<String> = MutableLiveData()
    var articleAbstract: MutableLiveData<String> = MutableLiveData()
    var articleByLine: MutableLiveData<String> = MutableLiveData()
    var articlePublishedDate: MutableLiveData<String> = MutableLiveData()

}