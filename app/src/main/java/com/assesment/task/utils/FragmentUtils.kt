package com.assesment.task.utils

import android.annotation.SuppressLint
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity

@SuppressLint("StaticFieldLeak")

/**
 * Fragment util class which takes care of all types of fragment transitions
 * Author: Muhammad Mudasir
 */
object FragmentUtils {

    private lateinit var context: AppCompatActivity

    /**
     * Constructor to initialize the context of the Activity in which fragment suppose to reside
     */
    fun init(context: AppCompatActivity) {
        this.context = context
    }

    /**
     * The method for replacing a new fragment
     * @param fragment: Fragment to be added
     * @param id: Fragment container ID
     * @param addToBackStack: Flag indicating whether to add to back stack or not
     */
    fun replaceFragment(fragment: Fragment, id: Int, addToBackStack: Boolean) {

        val fragmentManager = context.supportFragmentManager
        val transaction = fragmentManager.beginTransaction()

        if (addToBackStack)
            transaction.addToBackStack(fragment.javaClass.canonicalName)

        transaction.replace(id, fragment, fragment.javaClass.canonicalName)
        transaction.commit()
    }

}