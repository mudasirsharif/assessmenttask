package com.assesment.task.utils

import android.content.Context
import android.net.ConnectivityManager
import com.assesment.task.TaskApplicationClass

/**
 * The util class which contains the basic utility methods related to string manipulation, validation, etc.
 * Author: Muhammad Mudasir
 */
object MiscUtils {

    /**
     * Method to check if internet is connected
     * @return - true if internet is connected
     */
    fun isNetworkAvailable(): Boolean {

        val connectivityManager = TaskApplicationClass.applicationContext().getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetworkInfo = connectivityManager.activeNetworkInfo
        return activeNetworkInfo != null && activeNetworkInfo.isConnected

    }

}