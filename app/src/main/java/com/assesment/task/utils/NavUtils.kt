package com.assesment.task.utils

import com.assesment.task.R
import com.assesment.task.view.fragment.ArticleFragment
import com.assesment.task.view.fragment.ArticleListFragment

/**
 * Navigation Util class
 * Author: Muhammad Mudasir
 */
object NavUtils {

    fun pushArticleListFragment() {
        FragmentUtils.replaceFragment(ArticleListFragment(), R.id.fragment_container, false)
    }

    fun pushArticleFragment() {
        FragmentUtils.replaceFragment(ArticleFragment(), R.id.fragment_container, true)
    }
}
