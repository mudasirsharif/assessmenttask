package com.assesment.task.base

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.assesment.task.model.ErrorResponse

/**
 * This class serves as the Base ViewModel class. All other ViewModel classes should be extended from this
 * Author: Muhammad Mudasir
 */
abstract class BaseViewModel : ViewModel() {

    var loading: MutableLiveData<Boolean> = MutableLiveData()
    val serviceResponse: MutableLiveData<BaseModel> = MutableLiveData()
    val serviceErrorResponse: MutableLiveData<ErrorResponse> = MutableLiveData()
    val showDialog: MutableLiveData<String> = MutableLiveData()

    val toolBarTitle: MutableLiveData<String> = MutableLiveData()
}