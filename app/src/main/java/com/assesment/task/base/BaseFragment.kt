package com.assesment.task.base

import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.Fragment
import com.assesment.task.viewModel.SharedViewModel

/**
 * This class serves as the Base Fragment class. All other fragment classes should be extended from this
 * Author: Muhammad Mudasir
 */
abstract class BaseFragment : Fragment() {

    protected var sharedViewModel: SharedViewModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        /** Getting the SharedViewModel object from ViewModelProvider to retain and share between fragments **/
        sharedViewModel = activity?.let { ViewModelProviders.of(it).get(SharedViewModel::class.java) }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        /** Observes the Response and Error Response for Fragment **/
        observerServiceResponse()
        observerServiceErrorResponse()
        observerToShowDialog()
    }

    /**
     * Member function to observe the service response, which can be overridden by any fragment which doing any api call on server
     */
    open fun observerServiceResponse() {

    }

    /**
     * Member function to observe the service error response, which can be overridden by any fragment which doing any api call on server
     */
    open fun observerServiceErrorResponse() {

    }

    /**
     * Member function which will observe to show any error message in dialog
     */
    abstract fun observerToShowDialog()

}