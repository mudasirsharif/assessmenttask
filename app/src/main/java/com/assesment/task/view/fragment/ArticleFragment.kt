package com.assesment.task.view.fragment


import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.assesment.task.R
import com.assesment.task.base.BaseFragment
import com.assesment.task.databinding.FragmentArticleBinding
import com.assesment.task.model.Articles
import com.assesment.task.view.DialogFactory
import com.assesment.task.viewModel.ArticleFragmentViewModel

/**
 * Fragment for Google Maps
 * Author: Muhammad Mudasir
 */
class ArticleFragment : BaseFragment() {

    private lateinit var binding: FragmentArticleBinding
    private lateinit var articleFragmentViewModel: ArticleFragmentViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        /** ViewModel binding with view **/
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_article, container, false)

        /** Getting the ViewModel object from ViewModelProvider to retain the object in case of configuration changes **/
        articleFragmentViewModel = ViewModelProviders.of(this).get(ArticleFragmentViewModel::class.java)
        binding.viewModel = articleFragmentViewModel
        binding.setLifecycleOwner(this)

        /** setting the articles data from shared model **/
        articleFragmentViewModel.articleList = (sharedViewModel?.model as Articles).articles
        articleFragmentViewModel.articlePosition = sharedViewModel?.itemPosition as Int

        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        articleFragmentViewModel.renderArticleData()
        binding.articleWv.loadUrl(articleFragmentViewModel.articleUrl)
    }

    /**
     * Override method to observe for dialog
     */
    override fun observerToShowDialog() {

        articleFragmentViewModel.showDialog.observe(this, Observer {

            DialogFactory.showSimpleDialog(getString(R.string.error_string), it)
        })
    }
}