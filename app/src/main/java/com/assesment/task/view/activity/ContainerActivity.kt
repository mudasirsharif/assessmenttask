package com.assesment.task.view.activity

import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.assesment.task.R
import com.assesment.task.databinding.ActivityContainerBinding
import com.assesment.task.utils.FragmentUtils
import com.assesment.task.utils.NavUtils
import com.assesment.task.view.DialogFactory
import com.assesment.task.viewModel.ContainerActivityViewModel

/**
 * Activity which will serve as a container for all fragments
 * Author: Muhammad Mudasir
 */
class ContainerActivity : AppCompatActivity(){

    private lateinit var containerActivityViewModel: ContainerActivityViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        /** ViewModel binding with view **/
        val binding: ActivityContainerBinding = DataBindingUtil.setContentView(this, R.layout.activity_container)

        /** Getting the ViewModel object from ViewModelProvider to retain the object in case of configuration changes **/
        containerActivityViewModel = ViewModelProviders.of(this).get(ContainerActivityViewModel::class.java)
        binding.viewModel = containerActivityViewModel
        binding.setLifecycleOwner(this)

        /** Initializing the utility classes with activity context **/
        FragmentUtils.init(this)
        DialogFactory.init(this)

        /** To avoid pushing the fragment again in case of configuration changes **/
        if (savedInstanceState != null) {
            return
        }

        NavUtils.pushArticleListFragment()
    }
}