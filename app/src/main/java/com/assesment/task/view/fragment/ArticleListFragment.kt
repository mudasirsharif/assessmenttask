package com.assesment.task.view.fragment


import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.assesment.task.R
import com.assesment.task.base.BaseFragment
import com.assesment.task.databinding.FragmentArticleListBinding
import com.assesment.task.model.Articles
import com.assesment.task.utils.NavUtils
import com.assesment.task.view.DialogFactory
import com.assesment.task.viewModel.ArticleListFragmentViewModel


/**
 * Article List Fragment
 * Author: Muhammad Mudasir
 */
class ArticleListFragment : BaseFragment() {

    private lateinit var articleListFragmentViewModel: ArticleListFragmentViewModel
    private lateinit var binding: FragmentArticleListBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        /** ViewModel binding with view **/
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_article_list, container, false)

        /** Getting the ViewModel object from ViewModelProvider to retain the object in case of configuration changes **/
        articleListFragmentViewModel = ViewModelProviders.of(this).get(ArticleListFragmentViewModel::class.java)
        binding.viewModel = articleListFragmentViewModel
        binding.setLifecycleOwner(this)

        return binding.root

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        articleListFragmentViewModel.toolBarTitle.value = getString(R.string.articles_title)

        /** getting the article list **/
        if (articleListFragmentViewModel.articleItemViewModel.isEmpty()) {
            articleListFragmentViewModel.getArticleList()
            observerUserClickResponse()
        }


    }

    /**
     * Override method to observe for dialog
     */
    override fun observerToShowDialog() {
        articleListFragmentViewModel.showDialog.observe(this, Observer {

            DialogFactory.showSimpleDialog(getString(R.string.alert_string), it)
        })
    }

    /**
     * Override method to observe the service response to update the UI
     */
    override fun observerServiceResponse() {
        articleListFragmentViewModel.serviceResponse.observe(this, Observer {
            if (null != it) {
                sharedViewModel?.model = it
                articleListFragmentViewModel.renderArticleList(it as Articles)
            }
        })
    }

    /**
     * Override method to observe the error response
     */
    override fun observerServiceErrorResponse() {
        articleListFragmentViewModel.serviceErrorResponse.observe(this, Observer {

            DialogFactory.showSimpleDialog(getString(R.string.error_string), it?.errorMessage)

        })
    }

    /**
     * Method to observe the user click on article list item or Tool bar icon
     */
    private fun observerUserClickResponse() {
        articleListFragmentViewModel.userClickResponse.observe(this, Observer {
            if (null != it) {
                sharedViewModel?.itemPosition = it.toInt()
                NavUtils.pushArticleFragment()
            }
        })
    }
}