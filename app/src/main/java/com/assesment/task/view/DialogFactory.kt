package com.assesment.task.view

import android.annotation.SuppressLint
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity

@SuppressLint("StaticFieldLeak")

/**
 * This class can provide various alert dialogs
 * Author: Muhammad Mudasir
 */
object DialogFactory {

    private lateinit var context: AppCompatActivity

    /**
     * Constructor to initialize the context of the Activity to show the dialogs
     */
    fun init(context: AppCompatActivity) {
        this.context = context
    }

    /**
     * Method to show simple dialog
     * @param title - Title of dialog
     * @param message - Message of dialog
     */
    fun showSimpleDialog(title: String, message: String?) {

        val builder = AlertDialog.Builder(context)
        builder.setTitle(title)
        builder.setMessage(message)
        builder.setPositiveButton("Cancel") { dialog, _ ->
            dialog.dismiss()
        }
        val dialog: AlertDialog = builder.create()
        dialog.show()
    }

}