package com.assesment.task.model

/**
 * Model class to contain the error response from server
 * Author: Muhammad Mudasir
 */
data class ErrorResponse(var errorCode: String, var errorMessage: String)