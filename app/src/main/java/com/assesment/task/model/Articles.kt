package com.assesment.task.model

import com.google.gson.annotations.SerializedName
import com.assesment.task.base.BaseModel

/**
 * Data Model class for Article list
 * Author: Muhammad Mudasir
 */
data class Articles(@SerializedName("status") var status: String,
                    @SerializedName("copyright") var copyright: String,
                    @SerializedName("num_results") var numResults: Int,
                    @SerializedName("results") var articles: List<Article>) : BaseModel()

data class Article(@SerializedName("url") var url: String,
                   @SerializedName("adx_keywords") var adxKeywords: String,
                   @SerializedName("column") var column: Any,
                   @SerializedName("section") var section: String,
                   @SerializedName("byline") var byline: String,
                   @SerializedName("type") var type: String,
                   @SerializedName("title") var title: String,
                   @SerializedName("abstract") var abstract: String,
                   @SerializedName("published_date") var publishedDate: String,
                   @SerializedName("source") var source: String,
                   @SerializedName("id") var id: Long,
                   @SerializedName("asset_id") var assetId: Long,
                   @SerializedName("views") var views: Int,
                   @SerializedName("des_facet") var desFacet: Any,
                   @SerializedName("org_facet") var orgFacet: List<String>,
                   @SerializedName("per_facet") var perFacet: Any,
                   @SerializedName("geo_facet") var geoFacet: Any,
                   @SerializedName("media") var media: List<Media>)

data class Media(@SerializedName("type") var type: String,
                 @SerializedName("subtype") var subtype: String,
                 @SerializedName("caption") var caption: String,
                 @SerializedName("copyright") var copyright: String,
                 @SerializedName("approved_for_syndication") var approvedForSyndication: Int,
                 @SerializedName("media-metadata") var mediaMetaData: List<MediaMetadata>)

data class MediaMetadata(@SerializedName("url") var url: String,
                         @SerializedName("format") var format: String,
                         @SerializedName("height") var height: Int,
                         @SerializedName("width") var width: Int)