package com.assesment.task.dataManager

import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.QueryMap

/**
 * API Interface for all the service calls
 * Author: Muhammad Mudasir
 */
interface RetrofitClient {

    @GET("svc/mostpopular/v2/mostviewed/all-sections/7.json")
    fun getArticles(@QueryMap options: Map<String, String>): Call<ResponseBody>

}