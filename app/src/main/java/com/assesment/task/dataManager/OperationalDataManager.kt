package com.assesment.task.dataManager

import com.assesment.task.TaskApplicationClass
import javax.inject.Inject

/**
 * OperationalDataManager which is responsible for performing the service calls from one point
 * Author: Muhammad Mudasir
 */
class OperationalDataManager : DataManager() {

    @Inject
    lateinit var retrofitClient: RetrofitClient

    /**
     * Initializer block to inject the retrofitClient object
     */
    init {
        TaskApplicationClass.getAppComponent().inject(this)
    }

    /**
     * Article List Service Request
     * @param params - Article List Service Call Parameters
     * @param tClass - Model class as a generic object
     * @param responseListener - response listener
     */
    fun <T> articleServiceRequest(params: Map<String, String>, tClass: Class<T>, responseListener: ResponseListener<T>) {

        serviceRequest(retrofitClient.getArticles(params), tClass, responseListener)

    }

}