package com.assesment.task.dataManager

import android.util.Log
import com.google.gson.GsonBuilder
import com.assesment.task.TaskApplicationClass
import com.assesment.task.R
import com.assesment.task.model.ErrorResponse
import com.assesment.task.utils.MiscUtils
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException

/**
 * The class which is responsible to perform all the service requests.
 * Author: Muhammad Mudasir
 */
open class DataManager {

    private val tag: String? = DataManager::class.java.canonicalName

    /**
     * This method performs the service request by using retrofit call
     * @param retrofitCall - service call
     * @param responseListener - response callback
     */
    fun <T> serviceRequest(retrofitCall: Call<ResponseBody>, tClass: Class<T>, responseListener: ResponseListener<T>) {

        if (MiscUtils.isNetworkAvailable()) {
            retrofitCall.enqueue(object : Callback<ResponseBody> {

                override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {

                    if (response.isSuccessful) {

                        responseListener.onResponse(convertResponseToJavaObject(response, tClass), null)

                    }
                }

                override fun onFailure(call: Call<ResponseBody>, t: Throwable) {

                    responseListener.onResponse(null, ErrorResponse("", TaskApplicationClass.applicationContext().getString(R.string.retrofit_error)))

                }
            })

        } else {

            responseListener.onResponse(null, ErrorResponse("", TaskApplicationClass.applicationContext().getString(R.string.internet_error)))

        }

    }

    /**
     * Method to convert the Retrofit ResponseBody to corresponding model class (Pojo)
     * @param response - Service Response
     * @param tClass - Model class type
     * @return - Generic Plain old java object
     */
    private fun <T> convertResponseToJavaObject(response: Response<ResponseBody>, tClass: Class<T>): T? {

        var json: String? = null
        var genericJavaObject: T? = null
        try {
            json = response.body().string()
        } catch (e: IOException) {
            e.printStackTrace()
        }
        try {
            genericJavaObject = GsonBuilder().create().fromJson(json, tClass)

        } catch (e: Exception) {
            Log.d(tag, TaskApplicationClass.applicationContext().getString(R.string.parsing_error))
        }

        return genericJavaObject
    }

}