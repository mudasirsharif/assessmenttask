package com.assesment.task.dataManager

import com.assesment.task.model.ErrorResponse

/**
 * Response Listener on service response
 * Author: Muhammad Mudasir
 */
interface ResponseListener<in T> {
    fun onResponse(result: T?, error: ErrorResponse?)
}