package com.assesment.task.injection.module

import com.assesment.task.dataManager.OperationalDataManager
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * The Operational Data Manager Module for Dagger which provides dependencies to helper classes
 * Author: Muhammad Mudasir
 */
@Module
class OperationalDataManagerModule {

    @Provides
    @Singleton
    fun providesOperationalDataManager(): OperationalDataManager {
        return OperationalDataManager()
    }
}