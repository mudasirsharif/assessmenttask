package com.assesment.task.injection.module

import com.assesment.task.dataManager.RetrofitClient
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

/**
 * The Retrofit Module for Dagger which provides dependency object to Operational data Manager
 * Author: Muhammad Mudasir
 */
@Module
class RetrofitClientModule {

    private var retrofitClient: RetrofitClient
    private val baseUrl = "http://api.nytimes.com/"

    init {

        val builder = Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(
                        GsonConverterFactory.create()
                )

        val retrofit = builder.build()
        retrofitClient = retrofit.create(RetrofitClient::class.java)

    }

    @Singleton
    @Provides
    fun providesRetrofitClient(): RetrofitClient {
        return retrofitClient
    }

}