# AssessmentTask
This app is for the purpose of Task assessmennt. 
To Run the app, please follow the following steps.

1. Open Android studio and go to VCS -> Git -> Clone
2. Put the repository url, define the destination folder and hit Clone button
3. After cloning, open the app in Android Studio and Run the app

Gradle Version: 3.1.3
minSdkVersion: 18
targetSdkVersion: 27

- App follows the MVVM architecture
- App Conntains, one container activity and two fragment.
- ArticleListFragment is being used to show the api response as list.
- ArticleFragmennt is being used to show inndividual article with additional details.
- OOP concepts is being used to minimize the code reuse, specially when app size increases

Libraries used:

1. Dagger 2 for dependency injection
https://github.com/google/dagger
2. Retrofit as REST api
https://github.com/square/retrofit
3. Binding Collection View Adapters for recycler view
https://github.com/evant/binding-collection-adapter
4. Lottie animation by AirBnb
https://github.com/airbnb/lottie-android
5. Android Architecture components (Live Data, ViewModel Providers)
6. Data Binding


